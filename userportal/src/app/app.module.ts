import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginBoxComponent } from './login-box/login-box.component';
import { RegBoxComponent } from './reg-box/reg-box.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginBoxComponent,
    RegBoxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
